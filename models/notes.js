const mongoose = require('mongoose');
const User = require('./users');
const Schema = mongoose.Schema;

const notesSchema = new Schema({
    userId: String,
    offset: Number,
    limit: Number,
    count: Number,
    notes: [
      {
        type: new Schema({
          completed: { type: Boolean, default: false },
          userId: String,
          text: String,
          createdDate: Date
        }, { timestamps: true })
      }
    ]
}, { timestamps: true })


const Notes = mongoose.model('Notes', notesSchema);

module.exports = Notes;