const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const userSchema = new Schema({
    username: {
        type: String,
        required: [true, 'Please enter an username'],
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: [true, 'Please enter an password'],
        minlength: [6, 'Minimun password length is 6 characters']
    },
    createdDate: {
        
    }
}, {
    timestamps: true
});

// hash passwodrd by bcrypt
userSchema.pre('save', async function (next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

//static metod to login user
userSchema.statics.login = async function (username, password) {
    const user = await this.findOne({ username });
    
    if (user) {
        const auth = await bcrypt.compare(password, user.password);
        if (auth) {
            return user;
        }
        throw Error('Incorrect password')
    }
    throw Error('Incorrect username');
};

const User = mongoose.model('User', userSchema);

module.exports = User;