const { Router } = require('express');
const authController = require('../controllers/authController');
const { requireAuth, checkUser } = require('../middleware/authMiddleware');

const router = Router();

router.get('/api/auth/register', authController.register_get);
router.post('/api/auth/register', authController.register_post);

router.get('/api/auth/login', authController.login_get);
router.post('/api/auth/login', authController.login_post);
router.get('/api/auth/logout', authController.logout_get);

router.get('/api/users/me', requireAuth, authController.profile_get);
router.delete('/api/users/me', checkUser, authController.profile_delete);
router.patch('/api/users/me', authController.profile_patch);

router.get('/api/notes', requireAuth,  checkUser, authController.notes_get);
router.post('/api/notes', requireAuth, checkUser, authController.notes_post);
router.get('/api/notes/:id', requireAuth, authController.notes_get_id);
router.put('/api/notes/:id', requireAuth, authController.notes_put_id);
router.patch('/api/notes/:id', requireAuth, authController.notes_patch_id);
router.delete('/api/notes/:id', requireAuth, authController.notes_delete_id);

module.exports = router;
